# Nonprofits and the Leadership Structure of Mycorrhiza


This is a summarization of my research on nonprofit governance structures and general incorporation. First, please note this is not legal advice but merely personal internet research done recently, which should be regarded critically.

The objective of this research was to address whether, in anticipation of our planned launch with public releases or at least a minimum viable product by the end of December 2023, or early 2024, it is necessary to complete the incorporation process. Sid pointed out that delaying incorporation could have negative consequences. Hence, this inspired the deep dive into nonprofit structures.

My conclusion is that nonprofits could inadvertently curtail the effectiveness of left-leaning organizations in addressing systemic issues. This is not a deliberate sabotage but rather a consequence of overall ineptitude. In essence, the uniqueness of a nonprofit lies in its tax-exempt status granted by the government, which could be withdrawn for reasons that are often subjective.

I've categorized potential revenue for corporations into three groups. The first is financial investment, which includes both loans and stock sales. Neither is particularly advantageous for organizations fighting capitalism, as it leads to a venture capital (VC) growth mindset that may ultimately result in negative societal impacts such as gentrification.

The second is income from selling goods or services, the most favorable form of revenue generation. However, creating a profitable enterprise is challenging due to intense competition and the need for substantial cash and capital. Nonprofits often struggle more than private companies to scale and generate significant impact in this domain.

The third category encompasses donations and volunteering. Volunteers contribute their time, which, if valued monetarily, likely represents a significant portion of nonprofit revenue. Additionally, nonprofit professionals often accept lower salaries compared to industry standards, which can be viewed as a form of donation. 

The central problem for nonprofits is their reliance on tax-exempt status, which can be revoked and limits their funding methods. Nonprofits compete for finite donation pools, with each individual having a set donation budget. Consequently, the impact of a nonprofit is calculated, not just by the social good achieved with the funds raised, but also by accounting for what is lost when donations are diverted from more effective charities.

Since charities typically become less efficient as they grow, the inability to diversify funding sources by engaging in commercial activities is problematic. There are exceptions, such as the Girl Scouts selling cookies as a limited-time fundraising event without threatening commercial businesses. However, nonprofits are constrained in their commercial activities which must directly advance their founding goals. If unrelated business income exceeds 20% of a charity's total revenue, the IRS may revoke its tax-exempt status.

This creates a significant issue for our organization, as we intended part of Mariza's income to come from selling our software at market rates. A nonprofit structure will not accommodate our funding model. Nonetheless, there might be loopholes, since nonprofits can own for-profit subsidiaries. This structure is utilized by entities like OpenAI, where the nonprofit owns a for-profit subsidiary that generates substantial revenue.


When registering a nonprofit, the IRS typically grants tax-exempt status only to corporations with a board of directors lacking financial interest in the corporation. However, this often results in boards disconnected from the nonprofit's day-to-day realities. As paid staff cannot serve on the board, founders may exert significant influence without a board seat and might prefer board members who are less engaged and easier to influence.

A brief examination of nonprofits reveals many regulations intended to ensure proper operation, which can be both easily circumvented by those wishing to start malicious charities and burdensome to those aiming to create impactful organizations.

So why not operate as a traditional corporation pursuing donation funding? Generally, for-profits are expected to generate revenue through sales or venture capital; however, accepting donations as a corporation is legal, paying regular business tax on net profits.

Structural distinctions between tax-exempt nonprofits and for-profits encourage donations to nonprofits. Nonprofits have bylaws mandating a primary focus on societal benefits, and they are required to disclose their financial information publicly, which contributes to accountability and reports of corruption in the nonprofit sector.

Corporations can include similar provisions in their bylaws and voluntarily disclose financial data. Therefore, incorporating as a for-profit entity, emphasizing donations and volunteering, and providing transparency and trust regarding the use of funds, presents a viable path forward.

Incorporating with an alternative structure, such as a benefit corporation or as suggested by Sid, a cooperative, allows for pursuing various funding approaches while avoiding certain venture capital-related issues. These structures release shareholders from the obligation of maximizing profit and promote a social worldview among decision-makers.

While some in progressive circles express discomfort with capitalism, actively engaging with it can be perceived as distasteful. Yet, the pseudo-nonprofit approach appears more conducive to success. Studies suggest a higher survival rate for nonprofits than for-profits in the first ten years.

By adopting a flexible incorporation model, we can target all funding avenues, including venture capital, even under a workplace cooperative's limitations. The cooperative status doesn't provide the same tax exemption as NGOs but operates similarly to a pass-through corporation, allowing dividend distribution to members without standard income tax implications.

For our venture, we can consider different incorporation options, although currently I am leaning very heavily towards a so called "social benefit cooperative" under colorado code  (C.R.S. § 7-101-503(1)) that is an amalgamation of a benefit corperation and a coop, the biggest downside being that it might shut us off pretty heavily from reciving VC money. However it isn't even close to an immediate concern for us in my opinion. Our current priority is to launch a product, however rudimentary, by the end of 2023.

That concludes my thoughts for now. Bye